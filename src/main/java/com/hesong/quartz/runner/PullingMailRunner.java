package com.hesong.quartz.runner;

import java.io.IOException;

import static org.quartz.TriggerBuilder.*;
import static org.quartz.CronScheduleBuilder.*;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import com.hesong.mailEngine.tools.MailingLogger;
import com.hesong.quartz.job.PullingMailJob;

/**
 * 抓取邮件流程触发器
 * 
 * @author Bowen
 * 
 */
public class PullingMailRunner {

    public static Scheduler scheduler;

    public static void task(String interval, String begin, String end) throws IOException, SchedulerException {


        JobDetail job = JobBuilder
                .newJob(PullingMailJob.class)
                .withIdentity(PullingMailJob.PullingMailJobId,
                        PullingMailJob.JOB_GROUP).build();

        String hours = "*";
        if (!begin.equals("*")) {
            hours = begin + "-" + end;
        }
        String cronExpression = String.format("7 0/%s %s * * ?", interval,
                hours);
        MailingLogger.log.info("cronExpression: " + cronExpression);

        Trigger trigger = newTrigger()
                .withIdentity(PullingMailJob.PullingMailJobId,
                        PullingMailJob.JOB_GROUP)
                .withSchedule(cronSchedule(cronExpression)).forJob(job).build();

        scheduler = new StdSchedulerFactory("quartz.properties").getScheduler();
        // Add job to scheduler
        scheduler.scheduleJob(job, trigger);
        
//        JobExecutor je = new JobExecutor(ftp);
//        je.execute();
        
        scheduler.start();
    }
    
    public static void stop() throws SchedulerException{
        scheduler.shutdown(true);
    }
    
    public static void pause() throws SchedulerException{
        scheduler.pauseAll();
    }

    public static void resume() throws SchedulerException{
        scheduler.resumeAll();
    }
}

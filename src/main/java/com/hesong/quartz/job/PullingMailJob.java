package com.hesong.quartz.job;

import java.util.List;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.context.ApplicationContext;

import com.hesong.context.AppContext;
import com.hesong.email.bo.AccountBo;
import com.hesong.email.bo.MailBo;
import com.hesong.email.dao.BlackListDao;
import com.hesong.email.model.Account;
import com.hesong.mailEngine.pop.POP3;
import com.hesong.mailEngine.tools.MailingLogger;

/**
 * 抓取邮件工作流程
 * 
 * @author Bowen
 *
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class PullingMailJob implements Job {

	public static final String PullingMailJobId = "PullingMail";
	public static final String JOB_GROUP = "PullingMailGroup";

	@Override
	@SuppressWarnings("unchecked")
	public void execute(JobExecutionContext context)
			throws JobExecutionException {

		ApplicationContext ctx = AppContext.getApplicationContext();
		AccountBo accountBo = (AccountBo) ctx.getBean("accountBo");
		List<Account> list = (List<Account>) accountBo.getAllAccount();
		MailBo mailBo = (MailBo) ctx.getBean("mailBo");
		BlackListDao blackListDao = (BlackListDao) ctx.getBean("blackListDao");
		for (int i = 0; i < list.size(); i++) {
			int count = 0;
			count = POP3.getNewInboxMessages(list.get(i), mailBo, blackListDao);
			if (count > 0) {
				MailingLogger.log.info(count + " mails received.");
				;
			} else {
				MailingLogger.log.info("No more new mails.");
			}
		}

	}
}

package com.hesong.email.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/welcome")
public class HelloController {

    @RequestMapping(method= RequestMethod.GET, headers = "Accept=image/jpeg, image/jpg, image/png, image/gif")
    public @ResponseBody BufferedImage printWelcome(HttpServletRequest request){
       // String path = request.getSession().getServletContext().getRealPath("resources/images/attachment.png");
        try {
            InputStream in = request.getSession().getServletContext().getResourceAsStream("resources/images/attachment.png");
            BufferedImage img = ImageIO.read(in);
            return img;
//            ByteArrayOutputStream bos = new ByteArrayOutputStream();
//            ImageIO.write(img, "png", bos);
//            return bos.toByteArray();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        
    }
   
    
    @RequestMapping(value="/{name}", method=RequestMethod.GET)
    public String getName(@PathVariable String name, ModelMap model) {
        model.addAttribute("message", name);
        return "hello";
    }
    
    @RequestMapping(value="/resources/{name}", method = RequestMethod.GET)
    public @ResponseBody ArrayList<String> getShopInJSON(@PathVariable String name) {
        ArrayList<String> l = new ArrayList<String>();
        l.add(name);
        return l;
    }
}

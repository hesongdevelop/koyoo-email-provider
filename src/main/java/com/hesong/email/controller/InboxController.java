package com.hesong.email.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.net.ftp.FTPFile;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hesong.email.bo.MailBo;
import com.hesong.email.model.AttachmtFile;
import com.hesong.email.model.Mail;
import com.hesong.mailEngine.ftp.factories.FTPConnectionFactory;
import com.hesong.mailEngine.tools.EncodingExchange;

/**
 * 收件箱事件控制器
 * 
 * @author Bowen
 * 
 */
@Controller
@RequestMapping("/inbox")
public class InboxController {

	public static final int KB = 1 << 10;
	public static final int MB = 1 << 20;

	public static Logger InboxLogger = Logger.getLogger(InboxController.class);

	@Autowired
	private MailBo mailBo;

	public void setMailBo(MailBo mailBo) {
		this.mailBo = mailBo;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String index() {
		return "inbox";
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/resources", method = RequestMethod.GET)
	public @ResponseBody List getInbox() {
		return mailBo.getInbox();
	}

	@RequestMapping(value = "/resources/{id:\\d+}", method = RequestMethod.GET)
	public @ResponseBody Mail getMailByID(@PathVariable String id) {
		return mailBo.getMailByID(id);
	}

	@RequestMapping(value = "/showMail/{id}", method = RequestMethod.GET)
	public String showMailByID(Model model, @PathVariable String id) {
		Mail mail = mailBo.getMailByID(id);
		Mail reply = null;
		if (mail.getParent_id() != null && !mail.getParent_id().equals("")) { // 传来的ID为回复邮件的ID
			reply = mail;
			mail = mailBo.getMailByID(mail.getParent_id() + "");
		} else {
			reply = mailBo.getMailByParentID(mail.getId());
		}
		if (reply != null) {
			String content = reply.getContent();
			int index = content
					.indexOf("<br/><br/><br/><div style=\"font-size: 12px;font-family: Arial Narrow;padding:2px 0 2px 0;\">");
			if (index > 0)
				content = content.substring(0, index);
			reply.setContent(content);
		}
		model.addAttribute("mail", mail);
		model.addAttribute("reply", reply);

		if (mail.getAttachmtDir() != null) {
			String ftpDirPath = mail.getAttachmtDir();
			try {
				if (ftpDirPath.endsWith("Inline_images/")) {
					ftpDirPath = ftpDirPath.substring(0,
							ftpDirPath.indexOf("Inline_images/"));
				}
				FTPFile[] fileList = FTPConnectionFactory
						.getDefaultFTPConnection().listFiles(ftpDirPath);
				List<AttachmtFile> attchmtFiles = new ArrayList<AttachmtFile>();
				for (FTPFile file : fileList) {
					if (!file.isDirectory()) {
						// FTP协议里面，规定文件名编码为iso-8859-1，所以目录名或文件名需要转码
						String fileName = EncodingExchange
								.ISOToUTF8WithBase64(file.getName());
						attchmtFiles.add(new AttachmtFile(EncodingExchange
								.stringToHex(ftpDirPath + fileName), fileName,
								getSize(file.getSize())));
					}
				}
				model.addAttribute("attchmtFiles", attchmtFiles);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (reply != null && reply.getAttachmtDir() != null) {
			String ftpDirPath = reply.getAttachmtDir();
			try {
				if (ftpDirPath.endsWith("Inline_images/")) {
					ftpDirPath = ftpDirPath.substring(0,
							ftpDirPath.indexOf("Inline_images/"));
				}
				FTPFile[] fileList = FTPConnectionFactory
						.getDefaultFTPConnection().listFiles(ftpDirPath);
				List<AttachmtFile> attchmtFiles = new ArrayList<AttachmtFile>();
				for (FTPFile file : fileList) {
					// FTP协议里面，规定文件名编码为iso-8859-1，所以目录名或文件名需要转码
					if (!file.isDirectory()) {
						String fileName = EncodingExchange
								.ISOToUTF8WithBase64(file.getName());
						attchmtFiles.add(new AttachmtFile(EncodingExchange
								.stringToHex(ftpDirPath + fileName), fileName,
								getSize(file.getSize())));
					}
				}
				model.addAttribute("r_attchmtFiles", attchmtFiles);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return "showMail";
	}

	/**
	 * 根据给定的邮件ID查看对应的邮件
	 * 
	 * @param model
	 *            数据模型
	 * @param id
	 *            邮件ID
	 * @return JSP文件名
	 */
	@RequestMapping(value = "/readMail/{id}", method = RequestMethod.GET)
	public String readMail(ModelMap model, @PathVariable String id) {
		model.addAttribute("id", id);
		return "readMail-frame";
	}

	@RequestMapping(value = "/readMail/to/{id}", method = RequestMethod.GET)
	public String readMailByID(ModelMap model, @PathVariable String id) {
		InboxLogger.info("ID: " + id);
		Mail mail = mailBo.getMailByID(id);
		model.addAttribute("mail", mail);
		if (mail.getAttachmtDir() != null) {
			String ftpDirPath = mail.getAttachmtDir();
			try {
				if (ftpDirPath.endsWith("Inline_images/")) {
					ftpDirPath = ftpDirPath.substring(0,
							ftpDirPath.indexOf("Inline_images/"));
				}
				FTPFile[] fileList = FTPConnectionFactory
						.getDefaultFTPConnection().listFiles(ftpDirPath);
				List<AttachmtFile> attchmtFiles = new ArrayList<AttachmtFile>();
				for (FTPFile file : fileList) {
					if (!file.isDirectory()) {
						String name = EncodingExchange.ISOToUTF8WithBase64(file
								.getName());
						attchmtFiles.add(new AttachmtFile(EncodingExchange
								.stringToHex(ftpDirPath + name), name,
								getSize(file.getSize())));
					}
				}
				model.addAttribute("attchmtFiles", attchmtFiles);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return "readMail";
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/resources/{adrs:[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}}", method = RequestMethod.GET)
	public @ResponseBody List getMailByAdrs(@PathVariable String adrs) {
		return mailBo.getMailListByAdrs(adrs);
	}

	private String getSize(long i) {
		if (i >> 20 > 0) {
			return String.format("%1$,.2fMB", (double) i / MB);
		} else if (i >> 10 > 0) {
			return String.format("%1$,.2fKB", (double) i / KB);
		} else {
			return String.format("%dByte", i);
		}
	}
}

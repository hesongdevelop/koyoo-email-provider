package com.hesong.email.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.quartz.SchedulerException;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hesong.context.AppContext;
import com.hesong.mailEngine.ftp.factories.FTPConnectionFactory;
import com.hesong.quartz.runner.PullingMailRunner;

@Controller
@RequestMapping("/setting")
public class SettingController {

	@RequestMapping(method = RequestMethod.GET)
	public String show(ModelMap model) {

		Map<String, String> setting = parseSettingXML();
		if (setting != null) {
			model.addAttribute("host", setting.get("host"));
			model.addAttribute("port", setting.get("port"));
			model.addAttribute("username", setting.get("username"));
			model.addAttribute("password", setting.get("password"));
			model.addAttribute("begin", setting.get("begin"));
			model.addAttribute("interval", setting.get("interval"));
			model.addAttribute("end", setting.get("end"));
		}
		return "setting";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody String save(@RequestParam("ftp") String ftp,
			@RequestParam("runner") String runner) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			Map<String, String> ftpProps = mapper.readValue(ftp, Map.class);
			Map<String, String> runnerProps = mapper.readValue(runner,
					Map.class);
			String host = ftpProps.get("host");
			String port = ftpProps.get("port");
			String username = ftpProps.get("username");
			String password = ftpProps.get("password");
			String interval = runnerProps.get("interval");
			String begin = runnerProps.get("begin");
			String end = runnerProps.get("end");
			FTPConnectionFactory.initDefualtFTPclientConnection(host,
					Integer.parseInt(port), username, password);
			Document doc = DocumentHelper.createDocument();

			Element propsRoot = doc.addElement("setting");
			// FTP properties
			Element ftpElmt = propsRoot.addElement("ftp");
			Element hostElmt = ftpElmt.addElement("host");
			hostElmt.setText(host);
			Element portElmt = ftpElmt.addElement("port");
			portElmt.setText(port);
			Element usernameElmt = ftpElmt.addElement("username");
			usernameElmt.setText(username);
			Element passwordElmt = ftpElmt.addElement("password");
			passwordElmt.setText(password);

			// Quartz runner properties
			Element runnerElmt = propsRoot.addElement("runner");
			Element intervalElmt = runnerElmt.addElement("interval");
			intervalElmt.setText(interval);
			Element beginElmt = runnerElmt.addElement("begin");
			beginElmt.setText(begin);
			Element endElmt = runnerElmt.addElement("end");
			endElmt.setText(end);

			try {
				XMLWriter writer = new XMLWriter(new FileWriter(new File(
						getSettingFilePath())));
				writer.write(doc);
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "success";
	}

	@RequestMapping(value = "/run", method = RequestMethod.GET)
	public @ResponseBody String run() {
		Map<String, String> setting = parseSettingXML();
		try {
			FTPConnectionFactory.initDefualtFTPclientConnection(
					setting.get("host"), Integer.parseInt(setting.get("port")),
					setting.get("username"), setting.get("password"));


			// FTPClient ftp = FTPConnectionFactory.getDefaultFTPConnection();
			// if(ftp == null){
			// return "FTP connection failed.";
			// }
			// PullingMailRunner.task(ftp, setting.get("interval"),
			// setting.get("begin"), setting.get("end"));
			PullingMailRunner.task(setting.get("interval"),
					setting.get("begin"), setting.get("end"));
		} catch (IOException | SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Scheduler exception: " + e.toString();

		}
		return "success";
	}

	@RequestMapping(value = "/stop", method = RequestMethod.GET)
	public @ResponseBody String stop() {
		try {
			PullingMailRunner.stop();
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "failed";
		}
		return "success";
	}

	@RequestMapping(value = "/pause", method = RequestMethod.GET)
	public @ResponseBody String pause() {
		try {
			PullingMailRunner.pause();
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "failed";
		}
		return "success";
	}

	@RequestMapping(value = "/resume", method = RequestMethod.GET)
	public @ResponseBody String resume() {
		try {
			PullingMailRunner.resume();
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "failed";
		}
		return "success";
	}

	private Map<String, String> parseSettingXML() {
		File f = new File(getSettingFilePath());
		if (f.exists()) {
			System.out.println("File exist.");
			SAXReader reader = new SAXReader();
			Map<String, String> setting = new HashMap<String, String>();
			try {
				Document document = reader.read(f);
				Element rootElmt = document.getRootElement();
				Element ftpElmt = rootElmt.element("ftp");
				setting.put("host", ftpElmt.elementText("host"));
				setting.put("port", ftpElmt.elementText("port"));
				setting.put("username", ftpElmt.elementText("username"));
				setting.put("password", ftpElmt.elementText("password"));
				Element runnerElmt = rootElmt.element("runner");
				setting.put("interval", runnerElmt.elementText("interval"));
				setting.put("begin", runnerElmt.elementText("begin"));
				setting.put("end", runnerElmt.elementText("end"));
				return setting;
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		} else {
			System.out.println("File not exist.");
			return null;
		}
	}

	private String getSettingFilePath() {
		String fileName = "";
		ApplicationContext ctx = AppContext.getApplicationContext();
		try {
			fileName = ctx.getResource("").getFile().getAbsolutePath();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		// fileName = fileName + "\\WEB-INF\\classes\\properties\\setting.xml";
		fileName = fileName + "\\setting.xml";
		return fileName;
	}
}

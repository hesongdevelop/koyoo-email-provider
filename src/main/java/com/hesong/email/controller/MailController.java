package com.hesong.email.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.hesong.context.AppContext;
import com.hesong.email.bo.AccountBo;
import com.hesong.email.bo.MailBo;
import com.hesong.email.model.Account;
import com.hesong.email.model.Mail;
import com.hesong.email.model.SendMail;
import com.hesong.factories.PropertiesFactory;
import com.hesong.mailEngine.ftp.FTPEngine;
import com.hesong.mailEngine.ftp.factories.FTPConnectionFactory;
import com.hesong.mailEngine.smtp.SMTP;
import com.hesong.mailEngine.tools.EncodingExchange;
import com.hesong.mailEngine.tools.TimeHelper;

/**
 * 邮件控制器，新建邮件、发送、回复等功能
 * 
 * @author Bowen
 * 
 */
@Controller
@RequestMapping("/newMail")
public class MailController {

	private static String ATTR_TOP_DIR = PropertiesFactory
			.getAttrchmentTopDir();

	ApplicationContext ctx = AppContext.getApplicationContext();
	MailBo mailBo = (MailBo) ctx.getBean("mailBo");
	AccountBo accountBo = (AccountBo) ctx.getBean("accountBo");
	Logger MailLogger = Logger.getLogger(MailController.class);

	@RequestMapping(value = "/{account_id}", method = RequestMethod.GET)
	public String show(HttpServletResponse response,
			@PathVariable String account_id, String subject, String address,
			Model model, String sendResult) {
		model.addAttribute("account_id", account_id);
		model.addAttribute("subject", subject);
		model.addAttribute("address", address);
		return "newMail-frame";
	}

	@RequestMapping(value = "/to/{account_id}", method = RequestMethod.GET)
	public String to(HttpServletResponse response,
			@PathVariable String account_id, String subject, String address,
			Model model, String sendResult) {
		MailLogger.info("account_id: " + account_id);
		String email_addr = accountBo.findByID(account_id).getAccount();
		model.addAttribute("sender_email", email_addr);
		model.addAttribute("subject", subject);
		model.addAttribute("address", address);
		model.addAttribute("sendResult", sendResult);
		return "newMail";
	}

	/**
	 * 发送新邮件
	 * 
	 */
	@RequestMapping(value = "/send", method = RequestMethod.POST)
	public String send(
			String sender,
			String receivers,
			String subject,
			String content,
			@RequestParam(value = "uploadedfile", required = false) MultipartFile mulpartFile,
			String test, HttpServletResponse response,
			HttpServletRequest request) {

		SendMail sMail = new SendMail(subject, sender, receivers, content,
				new Date(), Mail.SEND_TYPE);
		if (sender == null || sender.equals("") || receivers == null
				|| receivers.equals("") || subject == null
				|| subject.equals("") || content == null || content.equals("")) {
		} else {
			ApplicationContext ctx = AppContext.getApplicationContext();
			AccountBo accountBo = (AccountBo) ctx.getBean("accountBo");
			Account account = accountBo.findByEmailAdrs(sMail.getSender());
			SMTP.sendMail(request, account, handleAtt(mulpartFile, sMail));
			sMail.setUnitID(account.getUnitID());
			mailBo.saveSendMail(sMail);
			return "sending-success";
		}
		return "sending-error";
	}

	/**
	 * 回复邮件 具备附件处理能力
	 */
	@RequestMapping("/reply")
	public String replyMail(
			String subject,
			String receiver,
			String sender,
			String content,
			String orgin_id,
			String orgin_send_date,
			String orgin_sender,
			String orgin_content,
			@RequestParam(value = "uploadedfile", required = false) MultipartFile mulpartFile,
			HttpServletRequest request) {
		try {
			SendMail sMail = new SendMail("回复：" + subject, orgin_sender,
					sender, content, new Date(), Mail.REPLY_TYPE);
			// sMail.setContent(sMail.getContent() +
			// origMailMaker(orgin_content));
			sMail.setContent(sMail.getContent());

			ApplicationContext ctx = AppContext.getApplicationContext();
			AccountBo accountBo = (AccountBo) ctx.getBean("accountBo");
			Account account = accountBo.findByEmailAdrs(sMail.getSender());
			SMTP.sendMail(request, account, handleAtt(mulpartFile, sMail));
			mailBo.saveReply(orgin_id, sMail);
			return "sending-success";
		} catch (Exception e) {
			MailLogger.error("回复接收到的邮件时发生错误:" + e.toString());
			return "sending-error";
		}

	}

//	@SuppressWarnings("rawtypes")
//	private String origMailMaker(String origMail) {
//		ObjectMapper mapper = new ObjectMapper();
//		try {
//			System.out.println("00000000" + origMail + "00000000");
//			Map origMailMap = mapper.readValue(origMail, Map.class);
//			String title = "<br/><br/><br/><div style=\"font-size: 12px;font-family: Arial Narrow;padding:2px 0 2px 0;\">-------&nbsp;原始邮件&nbsp;-------</div>";
//			String sender = "<h5>发信人：" + origMailMap.get("sender") + "</h5>";
//			String date = "<h5>时间：" + origMailMap.get("sentDate") + "</h5>";
//			String tag_begin = "<blockquote id=\"isReplyContent\" style=\"PADDING-LEFT: 1ex; MARGIN: 0px 0px 0px 0.8ex; BORDER-LEFT: #ccc 1px solid\">";
//			String tag_end = "</blockquote>";
//			return title + sender + date + tag_begin
//					+ origMailMap.get("origContent") + tag_end;
//		} catch (IOException e) {
//			MailLogger.error(e.toString());
//			return "";
//		}
//
//	}

	/**
	 * 处理附件
	 */
	public SendMail handleAtt(MultipartFile mulpartFile, SendMail sendMail) {
		String fileName = "";
		if (mulpartFile != null) {
			fileName = mulpartFile.getOriginalFilename();
			if (fileName != null && !fileName.equals("")) {
				String path = ATTR_TOP_DIR + "/" + sendMail.getSender() + "/"
						+ TimeHelper.getYearBaseDate(sendMail.getSendDate())
						+ "/"
						+ TimeHelper.getMonthBaseDate(sendMail.getSendDate())
						+ "/"
						+ TimeHelper.getDayBaseDate(sendMail.getSendDate())
						+ "/" + TimeHelper.getTimeStamp() + "/";
				FTPClient ftp = null;
				try {
					path = EncodingExchange.UTF8ToISO8859(path);
					ftp = FTPConnectionFactory.getDefaultFTPConnection();
					FTPEngine.uploadFile(ftp, path, fileName,
							mulpartFile.getInputStream());
					List<String> list = new ArrayList<String>();
					list.add(path
							+ EncodingExchange.UTF8ToISOWithBase64(fileName));
					sendMail.setAttachmtFiles(list);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return sendMail;
	}
}

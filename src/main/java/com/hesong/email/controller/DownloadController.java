package com.hesong.email.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hesong.mailEngine.ftp.factories.FTPConnectionFactory;
import com.hesong.mailEngine.tools.EncodingExchange;

/**
 * 下载事件控制器
 * 
 * @author Bowen
 * 
 */
@Controller
@RequestMapping("/download")
public class DownloadController {

	public static Logger downLoadLogger = Logger
			.getLogger(DownloadController.class);

	/**
	 * 从FTP服务器下载文件
	 * 
	 * @param filePath
	 *            文件在FTP服务器的绝对路径
	 * @param request
	 *            Http请求
	 * @param response
	 *            Http回复
	 * @throws IOException
	 *             读写异常
	 */
	@RequestMapping(value = "/attachment/{filePath}", method = RequestMethod.GET)
	public void downloadAttchmt(@PathVariable String filePath,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		String ftpFilePath = EncodingExchange.hexToString(filePath);
		String isoFilePath = ftpFilePath;
		try {

			// FTP协议里面，规定文件名编码为iso-8859-1

			String name = ftpFilePath.substring(
					ftpFilePath.lastIndexOf("/") + 1, ftpFilePath.length());
			String prefix = ftpFilePath.substring(0,
					ftpFilePath.lastIndexOf("/") + 1);

			isoFilePath = EncodingExchange.UTF8ToISO8859(prefix
					+ EncodingExchange.UTF8ToISOWithBase64(name));

			downLoadLogger.info("filePath=" + isoFilePath);
		} catch (UnsupportedEncodingException e) {
			downLoadLogger.info("字符编码转换失败: " + e.toString());
		}

		FTPClient ftp = FTPConnectionFactory.getDefaultFTPConnection();
		String filename = EncodingExchange.getFileName(ftpFilePath);
		if (isIEBrowser(request)) {
			// For IE
			response.addHeader("Content-Disposition", "attachment;filename="
					+ URLEncoder.encode(filename, "utf-8"));
		} else {
			response.addHeader("Content-Disposition", "attachment;filename*="
					+ I18nFilename(filename));
		}
		OutputStream out = response.getOutputStream();
		ftp.retrieveFile(isoFilePath, out);
		out.close();
	}

	@RequestMapping(value = "/image/{filePath}", method = RequestMethod.GET)
	public void downloadImage(@PathVariable String filePath,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		// XXX 此处上锁，否则图片显示BUG
		synchronized (this) {

			String ftpFilePath = EncodingExchange.hexToString(filePath);
			String isoFilePath = ftpFilePath;
			String name = null;
			try {
				name = ftpFilePath.substring(ftpFilePath.lastIndexOf("/") + 1,
						ftpFilePath.length());
				String prefix = ftpFilePath.substring(0,
						ftpFilePath.lastIndexOf("/") + 1);
				isoFilePath = EncodingExchange.UTF8ToISO8859(prefix
						+ EncodingExchange.UTF8ToISOWithBase64(name));

				FTPClient ftp = FTPConnectionFactory.getDefaultFTPConnection();
				String filename = EncodingExchange.getFileName(ftpFilePath);
				if (isIEBrowser(request)) {
					response.addHeader(
							"Content-Disposition",
							"inline;filename="
									+ URLEncoder.encode(filename, "utf-8"));
				} else {
					response.addHeader("Content-Disposition",
							"inline;filename*=" + I18nFilename(filename));
				}
				OutputStream out = response.getOutputStream();
				ftp.retrieveFile(isoFilePath, out);
				out.close();
				downLoadLogger.info("Image filePath=" + isoFilePath);
			} catch (UnsupportedEncodingException e) {
				downLoadLogger.info("字符编码转换失败: " + e.toString());
			}
		}
	}

	/**
	 * 生成跨浏览器文件名，除IE外
	 * 
	 * @param filename
	 *            文件名
	 * @return 格式化的文件名
	 * @throws UnsupportedEncodingException
	 */
	public String I18nFilename(String filename)
			throws UnsupportedEncodingException {
		String name = String.format("utf-8''%s",
				URLEncoder.encode(filename, "utf-8"));
		name = name.replace("+", "%20");
		return name;
	}

	public boolean isIEBrowser(HttpServletRequest request) {
		String header = request.getHeader("user-agent");
		if (header == null) {
			return false;
		}
		return header.indexOf("MSIE") > -1;
	}

}

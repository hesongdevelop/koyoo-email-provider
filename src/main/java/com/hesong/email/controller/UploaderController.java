package com.hesong.email.controller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;


@Controller
@RequestMapping("/upload")
public class UploaderController {

    Logger UploadLogger = Logger.getLogger(UploaderController.class);
    @RequestMapping(method = RequestMethod.GET)
    public String show() {
        return "upload";
    }

    @RequestMapping(value = "/attachmt", method = RequestMethod.POST)
    public String upload(@RequestParam("uploadedfile") MultipartFile mulpartFile,
            HttpServletResponse response) throws IOException {
        String fileName = "";
        UploadLogger.info(mulpartFile.isEmpty() ? "Empty" : "Full");
        UploadLogger.info("Size: " + mulpartFile.getSize());
        if (mulpartFile != null) {
            fileName = mulpartFile.getOriginalFilename();

        }
        UploadLogger.info("Name: " + fileName);
        UploadLogger.info("content-type: " + mulpartFile.getContentType());
        mulpartFile.transferTo(new File("D:\\" + fileName));

//        OutputStream out = new FileOutputStream(new File("D:\\test.sql"));
//        InputStream in = mulpartFile.getInputStream();
//        AttachmentPuller.copy(in, out);

        Map<String, String> res = new HashMap<String, String>();
        res.put("name", fileName);
        res.put("file", fileName);

        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        json = mapper.writeValueAsString(res);
        UploadLogger.info("JSON: "+json);
        
//        PrintWriter w = response.getWriter();
//        w.write("<html><body><textarea>"+json+"</textarea></body></html>");
        return "Success";
    }
}

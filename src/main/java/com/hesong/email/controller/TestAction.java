package com.hesong.email.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/t")
@Controller
public class TestAction {

	@RequestMapping("/ok")
	public void test(HttpServletResponse response){
		
	}
	@RequestMapping("/err")
	public String err(){
		return "sending-error";
	}
}

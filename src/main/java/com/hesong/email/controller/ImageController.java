package com.hesong.email.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/image")
public class ImageController {

    @RequestMapping(method = RequestMethod.GET, headers = "Accept=image/jpeg, image/jpg, image/png, image/gif")
    public @ResponseBody
    BufferedImage printWelcome(HttpServletRequest request) {
        // String path =
        // request.getSession().getServletContext().getRealPath("resources/images/attachment.png");
        try {
            InputStream in = request.getSession().getServletContext()
                    .getResourceAsStream("resources/images/电话.png");
            return ImageIO.read(in);
            // ByteArrayOutputStream bos = new ByteArrayOutputStream();
            // ImageIO.write((BufferedImage)ImageIO.read(in), "png", bos);
            // return bos.toByteArray();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

    }
}

package com.hesong.email.model;

public class FilterRule {
	
	public static final char RULE_AND = '1';
	public static final char RULE_OR = '0';
	
	private String id;
	private char is_rule_and;//1:and 0:or
	private String unitid;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public char getIs_rule_and() {
		return is_rule_and;
	}

	public void setIs_rule_and(char is_rule_and) {
		this.is_rule_and = is_rule_and;
	}

	public String getUnitid() {
		return unitid;
	}

	public void setUnitid(String unitid) {
		this.unitid = unitid;
	}

	
	
}

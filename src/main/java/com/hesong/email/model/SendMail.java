package com.hesong.email.model;

import java.util.Date;
import java.util.List;

public class SendMail {
    
    private String subject;
    private String sender;
    private String receivers;
    private List<String> inlineImages;
    private List<String> attachmtFiles;
    private String content;
    private Date sendDate;
    private String unitID;
    private int type;
    
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }
    public String getSender() {
        return sender;
    }
    public void setSender(String sender) {
        this.sender = sender;
    }
    public String getReceivers() {
        return receivers;
    }
    public void setReceivers(String receivers) {
        this.receivers = receivers;
    }
    public List<String> getInlineImages() {
        return inlineImages;
    }
    public void setInlineImages(List<String> inlineImages) {
        this.inlineImages = inlineImages;
    }
    public List<String> getAttachmtFiles() {
        return attachmtFiles;
    }
    public void setAttachmtFiles(List<String> attachmtFiles) {
        this.attachmtFiles = attachmtFiles;
    }
    
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * 
	 * @param subject 主题
	 * @param sender 发件人
	 * @param receivers 收件人
	 * @param content
	 * @param sendDate
	 */
	public SendMail(String subject, String sender, String receivers,
			String content,Date sendDate, int type) {
		super();
		this.subject = subject;
		this.sender = sender;
		this.receivers = receivers;
		this.content = content;
		this.sendDate = sendDate;
		this.type = type;
	}
	public SendMail() {
		super();
	}
	public String getUnitID() {
		return unitID;
	}
	public void setUnitID(String unitID) {
		this.unitID = unitID;
	}
    
}

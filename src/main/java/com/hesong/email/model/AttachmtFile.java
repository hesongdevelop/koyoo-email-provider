package com.hesong.email.model;

public class AttachmtFile {
    
    private String dirPath;
    private String fileName;
    private String size;
    

    public AttachmtFile(String dirPath, String fileName, String size) {
        super();
        this.dirPath = dirPath;
        this.fileName = fileName;
        this.size = size;
    }
    public String getDirPath() {
        return dirPath;
    }
    public void setDirPath(String dirPath) {
        this.dirPath = dirPath;
    }
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public String getSize() {
        return size;
    }
    public void setSize(String size) {
        this.size = size;
    }

    
    

}

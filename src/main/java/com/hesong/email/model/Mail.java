package com.hesong.email.model;

import java.io.Serializable;
import java.util.Date;

import javax.mail.Message;
import javax.mail.internet.MimeUtility;

import com.hesong.mailEngine.tools.RegExp;

public class Mail implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String uid;
	private String unitID;
	private String senderName;
	private String sender;
	private String receiver;
	private String subject;
	private Date sentDate;
	private int size;
	private String content;
	private String attachmtDir;
	private int type;

	public static int RECEIVED_TYPE = 0;
	public static int REPLY_TYPE = 1;
	public static int SEND_TYPE = 2;

	/**
	 * 原邮件:null，回复邮件:原邮件ID 2014年11月24日 17:28:30
	 */
	private String parent_id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getUnitID() {
		return unitID;
	}

	public void setUnitID(String unitID) {
		this.unitID = unitID;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAttachmtDir() {
		return attachmtDir;
	}

	public void setAttachmtDir(String attachmtDir) {
		this.attachmtDir = attachmtDir;
	}

	public String getParent_id() {
		return parent_id;
	}

	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Mail() {
		super();
	}

	public Mail(String uid, Account account, Message msg) {
		try {
			this.setUid(uid);
			this.setUnitID(account.getUnitID());
			String sender = msg.getFrom()[0].toString();
			if (sender.indexOf("utf-7") == -1) {
				sender = MimeUtility.decodeText(sender);
			} else {
				sender = RegExp.emailAdressMatcher(sender);
			}
			this.setSenderName(sender.split(" ")[0]);
			this.setSender(RegExp.emailAdressMatcher(sender));
			this.setReceiver(account.getAccount());
			this.setSubject(msg.getSubject());
			Date sent_date = msg.getSentDate();
			
			if (sent_date.getTime()>(new Date().getTime())) {
				this.setSentDate(new Date());
			} else {
				this.setSentDate(sent_date);
			}
			
			this.setSize(msg.getSize());
			this.setType(Mail.RECEIVED_TYPE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

package com.hesong.email.model;

public class BlackList {

	/**
	 * 发件人地址过滤规则
	 */
	public static final char TYPE_SENDER_EMAIL = '1';
	/**
	 * 邮件标题过滤规则
	 */
	public static final char TYPE_MAIL_SUBJECT = '0';
	
	private String id;
	private char is_for_address;//1:发件人邮箱过滤 0:邮件标题过滤
	private String keyword;
	private String unitid;
	
	public String getUnitid() {
		return unitid;
	}
	public void setUnitid(String unitid) {
		this.unitid = unitid;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public char getIs_for_address() {
		return is_for_address;
	}
	public void setIs_for_address(char is_for_address) {
		this.is_for_address = is_for_address;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
	public BlackList() {
		super();
	}
	
}

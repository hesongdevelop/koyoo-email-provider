package com.hesong.email.dao;

import java.util.List;

import com.hesong.email.model.BlackList;

public interface BlackListDao {
	
	List<BlackList> getAllSenderFilter(String unitid);//获取类型为0的对象
	List<BlackList> getAllTitleFilter(String unitid);//获取类型为1的对象
	boolean isAnd(String unitid);
	
}

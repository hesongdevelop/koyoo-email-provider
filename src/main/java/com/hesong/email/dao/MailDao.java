package com.hesong.email.dao;

import java.util.List;
import java.util.Set;

import com.hesong.email.model.Mail;

public interface MailDao {
    
    void save(Mail mail);
    void save(List<Mail> list);
    void delete(Mail mail);
    @SuppressWarnings("rawtypes")
    List findByUIDandEmail(String uid, String emailAdr);
    @SuppressWarnings("rawtypes")
    List getUIDList(String emailAdr);
    @SuppressWarnings("rawtypes")
    List getInbox();
    Mail getMailByID(String id);
    @SuppressWarnings("rawtypes")
    List getMailListByAdrs(String adrs);
	Mail getMailByParentID(String parent_id);
	Set<String> getAllUIds();
}

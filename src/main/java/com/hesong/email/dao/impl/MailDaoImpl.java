package com.hesong.email.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.hesong.email.dao.MailDao;
import com.hesong.email.model.Mail;

@SuppressWarnings("unchecked")
public class MailDaoImpl extends HibernateDaoSupport implements MailDao {

	private static Logger logger = Logger.getLogger(MailDaoImpl.class);

	public void save(Mail mail) {
		try {
			getHibernateTemplate().save(mail);
		} catch (Exception e) {
			logger.error("Svae mail error, cuased by: " + e.toString());
			e.printStackTrace();
		}
	}

	public void save(List<Mail> list) {
		try {
			getHibernateTemplate().saveOrUpdateAll(list);

		} catch (Exception e) {
			logger.error("Svae mail list error, cuased by: " + e.toString());
			e.printStackTrace();
		}
	}

	public void delete(Mail mail) {
		getHibernateTemplate().delete(mail);
	}

	@SuppressWarnings("rawtypes")
	public List findByUIDandEmail(String uid, String emailAdr) {
		List list = getHibernateTemplate().find(
				String.format(
						"from Mail m where m.uid='%s' and m.receiver='%s'",
						uid, emailAdr));
		return list;
	}

	@SuppressWarnings("rawtypes")
	public List getUIDList(String emailAdr) {
		List list = getHibernateTemplate().find(
				"select m.uid from Mail m where m.receiver=?", emailAdr);
		return list;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getInbox() {
		try {
			return getHibernateTemplate().find(
					"from Mail ORDER BY sentDate DESC");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList();
	}

	public Mail getMailByID(String id) {
		try {
			@SuppressWarnings("rawtypes")
			List list = getHibernateTemplate().find("from Mail m where m.id=?",
					id);
			return list.size() > 0 ? (Mail) list.get(0) : new Mail();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Mail();
	}

	@SuppressWarnings("rawtypes")
	public List getMailListByAdrs(String adrs) {
		try {
			List list = getHibernateTemplate().find(
					"from Mail m where m.receiver=?", adrs);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList();
	}

	@Override
	public Mail getMailByParentID(String parent_id) {

		List<Mail> list = getHibernateTemplate().find(
				"FROM Mail WHERE parent_id = ? ", parent_id);
		return (list.size() > 0) ? list.get(0) : null;
	}

	@Override
	public Set<String> getAllUIds() {
		List<String> ids = getHibernateTemplate().find("SELECT uid FROM Mail");
		return new HashSet<String>(ids);
	}
}

package com.hesong.email.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.hesong.email.dao.AccountDao;
import com.hesong.email.model.Account;

public class AccountDaoImpl extends HibernateDaoSupport implements AccountDao {

	private static Logger logger = Logger.getLogger(AccountDaoImpl.class);
	
	@Override
	public void save(Account a) {
		try {
			getHibernateTemplate().save(a);

		} catch (Exception e) {
			logger.error("Svae account error, cuased by: "
					+ e.toString());
			e.printStackTrace();
		}
	}

	@Override
	public void saveAll(List<Account> list) {
		try {
			getHibernateTemplate().saveOrUpdateAll(list);

		} catch (Exception e) {
			logger.error("Svae account list error, cuased by: "
					+ e.toString());
			e.printStackTrace();
		}
	}

	@Override
	public void update(Account a) {
		getHibernateTemplate().update(a);
	}

	@Override
	public void delete(Account a) {
		getHibernateTemplate().delete(a);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Account findByEmailAdrs(String emailAdrs) {
		List list = getHibernateTemplate().find(
				"from Account a where a.account=?", emailAdrs);
		return list.size() > 0 ? (Account) list.get(0) : null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getAllAccount() {
		try {
			List list = getHibernateTemplate().find("from Account");
			logger.info("Account size: " + list.size());
			return list;
		} catch (Exception e) {
			logger.info("Get account list failed, caused by: "
					+ e.toString());
			e.printStackTrace();
		}
		return new ArrayList();

	}

	@Override
	public Account findByID(String account_id) {
		Session session = getHibernateTemplate().getSessionFactory()
				.openSession();
		return (Account) session.createQuery("FROM Account WHERE id = :id")
				.setParameter("id", account_id).uniqueResult();
	}

}

package com.hesong.email.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.hesong.email.dao.BlackListDao;
import com.hesong.email.model.BlackList;
import com.hesong.email.model.FilterRule;

@SuppressWarnings("unchecked")
public class BlackListDaoImpl extends HibernateDaoSupport implements
		BlackListDao {

	@Override
	public List<BlackList> getAllSenderFilter(String unitid) {
		return getSession()
				.createQuery(
						"FROM BlackList WHERE is_for_address = :is_for_address AND unitid = :unitid")
				.setParameter("is_for_address", BlackList.TYPE_SENDER_EMAIL)
				.setParameter("unitid", unitid).list();
	}

	@Override
	public List<BlackList> getAllTitleFilter(String unitid) {
		return getSession()
				.createQuery(
						"FROM BlackList WHERE is_for_address = :is_for_address AND unitid = :unitid")
				.setParameter("is_for_address", BlackList.TYPE_MAIL_SUBJECT)
				.setParameter("unitid", unitid).list();
	}

	@Override
	/**
	 * 判断是否使用“与”规则
	 * true：使用“与”规则
	 * false:使用“或”规则
	 */
	public boolean isAnd(String unitid) {
		List<FilterRule> rules = getSession()
				.createQuery("FROM FilterRule WHERE unitid = :unitid")
				.setParameter("unitid", unitid).list();
		if (rules != null && rules.size() > 0) {
			if (rules.get(0).getIs_rule_and() == FilterRule.RULE_AND) {
				return true;
			}
		}
		return false;
	}

}

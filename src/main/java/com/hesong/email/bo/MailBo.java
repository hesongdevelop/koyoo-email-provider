package com.hesong.email.bo;

import java.util.List;
import java.util.Set;

import com.hesong.email.model.Mail;
import com.hesong.email.model.SendMail;

/**
 * 邮件业务对象借口
 * 
 * @author Bowen
 * 
 */
public interface MailBo {
    /**
     * 存储邮件
     * 
     * @param mail
     *            邮件对象
     */
    void save(Mail mail);

    /**
     * 存储邮件列表
     * 
     * @param list
     *            邮件列表
     */
    void save(List<Mail> list);

    /**
     * 删除邮件
     * 
     * @param mail
     *            待删除的邮件对象
     */
    void delete(Mail mail);

    /**
     * 根据UnitID和邮箱地址查找邮件
     * 
     * @param uid
     *            UnitID
     * @param emailAdr
     *            邮箱地址
     * @return 邮件
     */
    @SuppressWarnings("rawtypes")
    List findByUIDandEmail(String uid, String emailAdr);

    /**
     * 根据UnitID返回邮件
     * 
     * @param uid
     *            UnitID
     * @return 邮件对象
     */
    @SuppressWarnings("rawtypes")
    List getUIDList(String uid);

    /**
     * 获取收件箱内所有邮件
     * 
     * @return 邮件列表
     */
    @SuppressWarnings("rawtypes")
    List getInbox();

    /**
     * 根据邮件ID获取邮件
     * 
     * @param id
     *            邮件ID
     * @return 邮件对象
     */
    Mail getMailByID(String id);

    @SuppressWarnings("rawtypes")
    /**
     * 根据邮箱地址获取邮件列表
     * @param adrs 邮箱地址
     * @return 邮件列表
     */
    List getMailListByAdrs(String adrs);

	void saveReply(String parent_id, SendMail sendMail);

	Mail getMailByParentID(String id);

	void saveSendMail(SendMail sMail);

	Set<String> getAllUIds();

};

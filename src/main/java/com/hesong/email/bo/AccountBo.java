package com.hesong.email.bo;

import java.util.List;

import com.hesong.email.model.Account;

public interface AccountBo {

    void save(Account a);
    void update(Account a);
    void delete(Account a);
    Account findByEmailAdrs(String emailAdrs);
    @SuppressWarnings("rawtypes")
    List getAllAccount();
    void saveAll(List<Account> list);
	Account findByID(String account_id);
}

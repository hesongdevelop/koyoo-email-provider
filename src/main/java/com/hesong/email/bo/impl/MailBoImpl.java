package com.hesong.email.bo.impl;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.hesong.email.bo.MailBo;
import com.hesong.email.dao.MailDao;
import com.hesong.email.model.Mail;
import com.hesong.email.model.SendMail;
import com.hesong.mailEngine.tools.TimeHelper;

public class MailBoImpl implements MailBo {

	MailDao mailDao;

	public void setMailDao(MailDao mailDao) {
		this.mailDao = mailDao;
	}

	public void save(Mail mail) {
		this.mailDao.save(mail);
	}

	public void save(List<Mail> list) {
		this.mailDao.save(list);
	}

	public void delete(Mail mail) {
		mailDao.delete(mail);
	}

	@SuppressWarnings("rawtypes")
	public List findByUIDandEmail(String uid, String emailAdr) {
		return mailDao.findByUIDandEmail(uid, emailAdr);
	}

	@SuppressWarnings("rawtypes")
	public List getUIDList(String emailAdr) {
		return mailDao.getUIDList(emailAdr);
	}

	@SuppressWarnings("rawtypes")
	public List getInbox() {
		return mailDao.getInbox();
	}

	public Mail getMailByID(String id) {
		return mailDao.getMailByID(id);
	}

	@SuppressWarnings("rawtypes")
	public List getMailListByAdrs(String adrs) {
		return mailDao.getMailListByAdrs(adrs);
	}

	/**
	 * 将回复存入数据库 2014年11月24日 17:41:50
	 */
	@Override
	public void saveReply(String parent_id, SendMail replyMail) {
		String content = replyMail.getContent();
		Mail parent = getMailByID(parent_id);
		Mail reply = new Mail();
		reply.setUid("reply");
		reply.setUnitID(parent.getUnitID());
		reply.setSubject(parent.getSubject());
		reply.setReceiver(parent.getSender());
		reply.setSenderName(parent.getReceiver());
		reply.setSender(parent.getReceiver());
		reply.setSentDate(new Date());
		reply.setSize((content.getBytes().length % 1024));
		reply.setContent(content);
		reply.setParent_id(parent.getId());
		reply.setType(replyMail.getType());
		if (replyMail.getAttachmtFiles()!=null&&replyMail.getAttachmtFiles().size()>0) {
			String filepath = replyMail.getAttachmtFiles().get(0);
			
			System.out.println(filepath);
			
			reply.setAttachmtDir(filepath.substring(0,filepath.lastIndexOf("/")+1));
		}
		mailDao.save(reply);
	}

	@Override
	public Mail getMailByParentID(String parent_id) {
		return mailDao.getMailByParentID(parent_id);
	}

	@Override
	public void saveSendMail(SendMail sMail) {
		Mail mail = new Mail();
		mail.setContent(sMail.getContent());
		mail.setSubject(sMail.getSubject());
		mail.setSender(sMail.getSender());
		mail.setReceiver(sMail.getReceivers());
		mail.setSentDate(sMail.getSendDate());
		if(sMail.getAttachmtFiles()!=null&&sMail.getAttachmtFiles().size()>0){
			String filepath = sMail.getAttachmtFiles().get(0);
			mail.setAttachmtDir(filepath.substring(0,filepath.lastIndexOf("/")+1));
		}
		mail.setUid(TimeHelper.getTimeStamp());
		mail.setSenderName(sMail.getSender());
		mail.setSize(sMail.getContent().length());
		mail.setUnitID(sMail.getUnitID());
		mail.setType(sMail.getType());
		mailDao.save(mail);
	}

	@Override
	public Set<String> getAllUIds() {
		return mailDao.getAllUIds();
	}

}

package com.hesong.factories;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.springframework.context.ApplicationContext;

import com.hesong.context.AppContext;
import com.hesong.email.model.Account;

public class PropertiesFactory {

	public static Properties POP3Factory(Account e) {
		Properties props = new Properties();
		props.setProperty("mail.store.protocol", "pop3");
		props.setProperty("mail.pop3.host", e.getReceiveServer());
		// props.setProperty("mail.pop3.port", "");
		if (e.getSsl() == '1')
			props.setProperty("mail.pop3.ssl.enable", "true");
		return props;
	}

	/**
	 * 获取附件存储位置的顶级目录配置
	 * @return
	 */
	public static String getAttrchmentTopDir(){
		String topDir = null;
		Properties properties = new Properties();
		try {
			
			System.out.println(getPropertiesFilePath("attachment_folder.properties"));
			
			properties.load(new FileInputStream(new File(getPropertiesFilePath("attachment_folder.properties"))));
			topDir = properties.getProperty("base_folder");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return topDir;
	}
	
	/**
	 * 获取 \\WEB-INF\\classes\\properties  目录中的配置文件
	 * @param fileName
	 * @return
	 */
	public static String getPropertiesFilePath(String fileName) {
		String path = "";
		ApplicationContext ctx = AppContext.getApplicationContext();
		try {
			path = ctx.getResource("").getFile().getAbsolutePath();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		path = path + "/WEB-INF/classes/properties/" + fileName;
		return path;
	}

}

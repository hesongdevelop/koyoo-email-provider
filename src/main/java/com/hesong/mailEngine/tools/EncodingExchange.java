package com.hesong.mailEngine.tools;

import java.io.UnsupportedEncodingException;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.net.util.Base64;

public class EncodingExchange {

	public static final String REPLACE_CHAR = "_";// BASE64编码外的字符，避免混淆
	public static final String DEINED_CHAR = "/";// 违反系统命名规则的字符

	public static String ISO8859ToUTF8(String iso8859)
			throws UnsupportedEncodingException {
		return new String(iso8859.getBytes("ISO-8859-1"), "UTF-8");
	}

	public static String UTF8ToISO8859(String utf8)
			throws UnsupportedEncodingException {
		return new String(utf8.getBytes("UTF-8"), "ISO-8859-1");
	}

	public static String stringToHex(String s) {
		return DatatypeConverter.printHexBinary(s.getBytes());
	}

	public static String hexToString(String hex) {
		return new String(DatatypeConverter.parseHexBinary(hex));
	}

	/**
	 * UTF8经base64编码再转iso
	 * 
	 * @param utf8String
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String UTF8ToISOWithBase64(String utf8String)
			throws UnsupportedEncodingException {
		byte[] encode = Base64.encodeBase64(utf8String.getBytes("UTF-8"));
		String iso_str = new String(encode, "ISO-8859-1");
		return iso_str.replace(DEINED_CHAR, REPLACE_CHAR);
	}

	/**
	 * ISO经base64编码再转UTF8
	 * 
	 * @param isoString
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String ISOToUTF8WithBase64(String isoString)
			throws UnsupportedEncodingException {
		isoString = isoString.replace(REPLACE_CHAR, DEINED_CHAR);
		byte[] b = Base64.decodeBase64(isoString.getBytes("ISO-8859-1"));
		return new String(b, "UTF-8");
	}

	/**
	 * 根据给定的路径获取文件名
	 * 
	 * @param path
	 *            文件的绝对路径
	 * @return 文件名
	 */
	public static String getFileName(String path) {
		String[] subs = path.split("/");
		return subs[subs.length - 1];
	}
}

package com.hesong.mailEngine.tools;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class Test {
	
	public static void main(String[] args) throws UnsupportedEncodingException, ParseException {
		SimpleDateFormat fmt = new SimpleDateFormat(
				"EEE MMM dd HH:mm:ss 'CST' yyyy", Locale.US);
		String time = "Wed Apr 15 22:03:38 CST 2015";
		Date date = fmt.parse(time);
		
		System.out.println(TimeHelper.paseUsTime(date));
		
	}
	
}

package com.hesong.mailEngine.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

public class TimeHelper {

	public static String formatDateYYMMDD(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		return simpleDateFormat.format(date);
	}

	/**
	 * 根据日期获取年份
	 */
	public static String getYearBaseDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.YEAR) + "";
	}

	/**
	 * 根据日期获取月份
	 */
	public static String getMonthBaseDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return (calendar.get(Calendar.MONTH) + 1) + "";
	}

	/**
	 * 根据日期获取具体哪天
	 */
	public static String getDayBaseDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_MONTH) + "";
	}

	/**
	 * 获取时间戳
	 * 
	 * @return
	 */
	public static String getTimeStamp() {
		String pre = new Date().getTime() + "";
		Random random = new Random();
		String append = "";
		for (int i = 0; i < 3; i++) {
			append += random.nextInt(10);
		}
		return pre + append;
	}

	public static Date paseUsTime(Date us_time) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		SimpleDateFormat local = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		String pase_str = simpleDateFormat.format(us_time);
		try {
			return local.parse(pase_str);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

}

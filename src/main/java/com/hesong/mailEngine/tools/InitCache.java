package com.hesong.mailEngine.tools;

import java.io.File;

import javax.annotation.Resource;
import javax.servlet.ServletContext;

import org.springframework.stereotype.Repository;

@Repository
public class InitCache {

	@Resource
	private ServletContext servletContext;

	public void mkCache() {
		String filePath = servletContext.getRealPath("/") + "//cache";
		File file = new File(filePath);
		if (!file.exists()) {
			file.mkdir();
		}
	}

}

package com.hesong.mailEngine.tools;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;

public class AttachmentPuller {
    
	private static Logger logger = Logger.getLogger(AttachmentPuller.class);
    /**
     * 文件拷贝，在用户进行附件下载的时候，可以把附件的InputStream传给用户进行下载
     * 
     * @param is
     * @param os
     * @throws IOException
     */
    public static void copy(InputStream is, OutputStream os) throws IOException {
        byte[] bytes = new byte[1024];
        int len = 0;
        logger.info("Pulling file...");
        while ((len = is.read(bytes)) != -1) {
            os.write(bytes, 0, len);
        }
        logger.info("Done!");
        if (os != null)
            os.close();
        if (is != null)
            is.close();
    }

}

package com.hesong.mailEngine.tools;

import java.io.File;


public class FileHelper {

	/**
	 * 清空缓存目录下的文件
	 * @param cacheDir 目录路径如:"D\\cache\\"
	 */
	public static void clearCache(String cacheDir) {
		File file = new File(cacheDir);
		if (file.isDirectory()) {
			for (String str : file.list()) {
				File file2 = new File(cacheDir + str);
				file2.delete();
			}
		}
	}

}

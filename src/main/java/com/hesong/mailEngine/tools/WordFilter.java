package com.hesong.mailEngine.tools;

import java.util.List;

import com.hesong.email.model.BlackList;

public class WordFilter {

	/**
	 * 检查是否包含关键字
	 * 
	 * @param text
	 * @return
	 */
	public static boolean inBlackList(String text, List<BlackList> list) {
		for (BlackList blackList : list) {
			if (text.indexOf(blackList.getKeyword()) >= 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 是否接收该邮件
	 * 
	 * @return
	 */
	public static boolean isReceive(String sender_email,
			List<BlackList> sender_blacklist, String title,
			List<BlackList> title_blacklist, boolean isAnd) {
		Boolean sender_in = inBlackList(sender_email, sender_blacklist);
		Boolean title_in = inBlackList(title, title_blacklist);
		if (isAnd) {// 两个判断条件同时生效
			if (sender_in && title_in) {
				return false;
			} else {
				return true;
			}
		} else {
			if (sender_in || title_in) {
				return false;
			} else {
				return true;
			}
		}
	}

}

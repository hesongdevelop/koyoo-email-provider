package com.hesong.mailEngine.tools;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

public class PathHelper {

	private static Logger pathLogger = Logger.getLogger(PathHelper.class);
	
	/**
	 * 读取User.dir 项目目录
	 * @return
	 */
	public static String getCacheDir() {
		try {
			pathLogger.info("调用了读取系统目录的方法");
			return System.getProperty("user.dir") + "/cache/";
		} catch (Exception e) {
			pathLogger.error("读取路径的错误");
			return null;
		}
	}
	/**
	 * 获取web项目根目录下的cache目录
	 * @param request
	 * @return
	 */
	public static String getWebROOT(HttpServletRequest request){
		pathLogger.info("读取request中的项目路径");
		return request.getSession().getServletContext().getRealPath("/")+"/cache/";
	}

}

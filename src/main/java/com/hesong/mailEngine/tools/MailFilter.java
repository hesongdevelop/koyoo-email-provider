package com.hesong.mailEngine.tools;

import java.util.Set;

/**
 * 挑选POP3未读邮件
 * 
 * @author yooclimb
 *
 */
public class MailFilter {

	/**
	 * 判断是否收到新邮件
	 * 
	 * @param inbox_mail_ids
	 *            收件箱中所有邮件的ID
	 * @param db_mail_ids
	 *            数据库中已经收到的所有邮件的ID
	 * @return 新邮件的ID集合
	 */
	public static Set<String> filter(Set<String> inbox_mail_ids,
			Set<String> db_mail_ids) {
		if (inbox_mail_ids.removeAll(db_mail_ids)) {
			return inbox_mail_ids;
		}
		return null;
	}

	/**
	 * 判断该邮件是否已经被保存在数据库中
	 * @param uid
	 * @param db_mail_uids
	 * @return
	 */
	public static boolean isSaved(String uid, Set<String> db_mail_uids) {
		if (db_mail_uids.contains(uid)) {
			return true;
		}
		return false;
	}

}

package com.hesong.mailEngine.smtp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

import sun.misc.BASE64Encoder;

import com.hesong.email.model.Account;
import com.hesong.email.model.SendMail;
import com.hesong.mailEngine.ftp.factories.FTPConnectionFactory;
import com.hesong.mailEngine.pop.POP3;
import com.hesong.mailEngine.tools.EncodingExchange;
import com.hesong.mailEngine.tools.FileHelper;
import com.hesong.mailEngine.tools.PathHelper;
import com.sun.mail.pop3.POP3Folder;

public class SMTP {

	public static Logger SMTPLogger = Logger.getLogger(SMTP.class);

	public static String INLINE_IMAGE_PREFIX = "inline_image_";

	public static void sendMail(HttpServletRequest request, Account account,
			SendMail sMail) {
		MimeMultipart mulPart = null;
		try {
			mulPart = parseMail(request, sMail);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		Properties props = new Properties();
		props.put("mail.smtp.host", account.getSendServer());
		props.put("mail.smtp.auth", "true");
		Session session = Session.getInstance(props, null);
		MimeMessage mimeMsg = new MimeMessage(session);
		try {
			mimeMsg.setFrom(new InternetAddress(account.getAccount(), account
					.getAccount().split("@")[0], "UTF-8"));
			mimeMsg.setSubject(sMail.getSubject(), "UTF-8");
			mimeMsg.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(sMail.getReceivers()));
			mimeMsg.setContent(mulPart);
			mimeMsg.saveChanges();

			Transport transport = session.getTransport("smtp");
			transport.connect(account.getSendServer(), account.getAccount(),
					account.getPassword());
			transport.sendMessage(mimeMsg,
					mimeMsg.getRecipients(Message.RecipientType.TO));

			transport.close();
			FileHelper.clearCache(PathHelper.getWebROOT(request));

		} catch (UnsupportedEncodingException | MessagingException e) {
			e.printStackTrace();
		}

	}

	// ******************* DO NOT USE FOR NOW *******************
	public static void replyMail(HttpServletRequest request, Account account,
			SendMail sMail, Message msg) throws MessagingException {
		// MimeMultipart mulPart = parseMail(sMail);
		Properties props = new Properties();
		props.put("mail.smtp.host", account.getSendServer());
		props.put("mail.smtp.auth", "true");
		Session session = Session.getInstance(props, null);

		SMTPLogger.info("TO: "
				+ InternetAddress.toString(msg
						.getRecipients(Message.RecipientType.TO)));
		SMTPLogger.info("REPLY TO: "
				+ InternetAddress.toString(msg.getReplyTo()));
		String from = InternetAddress.toString(msg
				.getRecipients(Message.RecipientType.TO));
		MimeMessage reply = (MimeMessage) msg.reply(false);
		MimeMultipart mulPart = parseMail(request, sMail);
		reply.setContent(mulPart);
		reply.setFrom(new InternetAddress(from));
		reply.setReplyTo(msg.getReplyTo());
		reply.saveChanges();
		Transport transport = session.getTransport("smtp");
		transport.connect(account.getSendServer(), account.getAccount(),
				account.getPassword());
		transport.sendMessage(reply, reply.getAllRecipients());
		POP3.closePOP3connection(null, (POP3Folder) reply.getFolder());
		transport.close();
	}

	@SuppressWarnings("restriction")
	public static MimeMultipart parseMail(HttpServletRequest request,
			SendMail sMail) throws MessagingException {
		MimeMultipart mulPart = new MimeMultipart();
		BodyPart contentPart = new MimeBodyPart();
		contentPart.setContent(sMail.getContent(), "text/html;charset=UTF-8");
		mulPart.addBodyPart(contentPart, 0);

		if (sMail.getAttachmtFiles() != null) {
			for (String attachmtPath : sMail.getAttachmtFiles()) {
				BodyPart attachmtPart = new MimeBodyPart();

				String isoFilePath = attachmtPath;
				FileOutputStream fos = null;
				FTPClient ftp = null;
				try {
					ftp = FTPConnectionFactory.getDefaultFTPConnection();
					String filename = EncodingExchange
							.getFileName(attachmtPath);
					filename = EncodingExchange.ISOToUTF8WithBase64(filename);
					File file = new File(PathHelper.getWebROOT(request)
							+ filename);
					fos = new FileOutputStream(file);
					ftp.retrieveFile(isoFilePath, fos);
					fos.flush();
					FileDataSource fds = new FileDataSource(file); // 得到数据源
					
					attachmtPart.setDataHandler(new DataHandler(fds)); // 得到附件本身并至入BodyPart
					attachmtPart.setFileName(MimeUtility.encodeText(fds.getName()));

				} catch (IOException e) {
					e.printStackTrace();
					SMTPLogger.error(" error while sending attchment: "
							+ e.toString());
				} finally {
					try {
						fos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				mulPart.addBodyPart(attachmtPart);
			}
		}

		return mulPart;
	}

	public static String getFileNameByURL(String url) {
		String[] pathSplit = url.split("/");
		String fileNameHex = pathSplit[pathSplit.length - 1];
		return EncodingExchange.getFileName(EncodingExchange
				.hexToString(fileNameHex));
	}

}

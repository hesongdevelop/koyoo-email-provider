package com.hesong.mailEngine.pop;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

import com.hesong.email.bo.MailBo;
import com.hesong.email.dao.BlackListDao;
import com.hesong.email.model.Account;
import com.hesong.email.model.BlackList;
import com.hesong.email.model.Mail;
import com.hesong.factories.PropertiesFactory;
import com.hesong.mailEngine.ftp.FTPEngine;
import com.hesong.mailEngine.ftp.factories.FTPConnectionFactory;
import com.hesong.mailEngine.tools.EncodingExchange;
import com.hesong.mailEngine.tools.MailFilter;
import com.hesong.mailEngine.tools.RegExp;
import com.hesong.mailEngine.tools.TimeHelper;
import com.hesong.mailEngine.tools.WordFilter;
import com.sun.mail.pop3.POP3Folder;

/**
 * POP3协议
 * 
 * @author Bowen
 * 
 */
public class POP3 {

	public final static String INBOX = "INBOX";
	public final static String POP3 = "pop3";
	public final static String TEXT_HTML_CONTENT = "text/html";
	public final static String TEXT_PLAIN_CONTENT = "text/plain";
	public final static String MULTIPART = "multipart/*";
	public final static String OCTET_STREAM = "application/octet-stream";
	public final static String CONTENT_ID = "Content-ID";
	private static String ATTR_TOP_DIR = PropertiesFactory
			.getAttrchmentTopDir();

	public static Logger POP3Logger = Logger.getLogger(POP3.class);

	public static SimpleDateFormat sdf_receive = new SimpleDateFormat(
			"yyyy-MM-dd_HH-mm-ss");
	public static SimpleDateFormat sdf_today = new SimpleDateFormat(
			"yyyy-MM-dd");

	/**
	 * 收取邮件
	 * 
	 * @param account
	 * @param mailBo
	 * @param blackListDao
	 */
	public static int getNewInboxMessages(Account account, MailBo mailBo,
			BlackListDao blackListDao) {
		String unit_id = account.getUnitID();
		Store store = null;
		Folder floder = null;
		int count = 0;
		try {
			store = POP3connection(account);
			floder = store.getFolder("INBOX");
			floder.open(Folder.READ_WRITE);
			Message[] messages = floder.getMessages();
			Set<String> db_mail_uids = mailBo.getAllUIds();

			POP3Logger.info(" inbox mail count is :" + messages.length
					+ " and db mail count is :" + db_mail_uids.size());

			List<BlackList> sender_blacklist = blackListDao
					.getAllSenderFilter(unit_id);
			List<BlackList> title_blacklist = blackListDao
					.getAllTitleFilter(unit_id);
			boolean isAnd = blackListDao.isAnd(unit_id);

			for (Message message : messages) {

				if (message.getFrom() != null && message.getFrom().length != 0) {
					String sender = message.getFrom()[0].toString();
					if (sender.indexOf("utf-7") == -1) {
						sender = MimeUtility.decodeText(sender);
					} else {
						sender = RegExp.emailAdressMatcher(sender);
					}
					String sender_email = RegExp.emailAdressMatcher(sender);
					String title = message.getSubject();

					if (WordFilter.isReceive(sender_email, sender_blacklist,
							title, title_blacklist, isAnd)) {// 完成关键字过滤
						// 添加符合条件的邮件ID到判断去重集合，取出新收到的邮件
						String uid = ((POP3Folder) floder).getUID(message);
						if (!MailFilter.isSaved(uid, db_mail_uids)) {// 该邮件尚未保存,保存到数据库
							Mail mail = new Mail(uid, account, message);
							Object content = "";
							if(message.getContentType().indexOf("utf-7")<0){
								content = message.getContent();
							}
							if (content != null
									&& content instanceof MimeMultipart) {
								parseMultipartByMimeType(
										(MimeMultipart) content, mail);
							} else {
								mail.setContent(content.toString());
							}
							POP3Logger
									.info("----received a new filtered mail----");
							POP3Logger.info(" Main Subject :"
									+ mail.getSubject());
							POP3Logger.info(" Sender :" + sender);
							POP3Logger.info(" Mail UID :" + mail.getUid());
							mailBo.save(mail);
							db_mail_uids.add(mail.getUid());
							POP3Logger
									.info("----Insert into db successful----");
							count += 1;
						}
					}
				}

			}
			closePOP3connection(store, floder);
		} catch (Exception e) {
			e.printStackTrace();
			POP3Logger.info("POP3 connection exception: " + e.toString());
		}
		return count;
	}

	public static void parseMultipartByMimeType(Multipart multipart, Mail mail)
			throws MessagingException, IOException {

		for (int i = 0; i < multipart.getCount(); i++) {
			BodyPart bodyPart = multipart.getBodyPart(i);
			String disposition = bodyPart.getDisposition();
			String content_id = getContentid(bodyPart);// 邮件正文中的图片

			if (content_id != null) {
				disposition = BodyPart.INLINE;
			}

			if (disposition != null
					&& (disposition.equalsIgnoreCase(BodyPart.INLINE) || disposition
							.equalsIgnoreCase(BodyPart.ATTACHMENT))) {
				String fileName = MimeUtility.decodeText("attr_file");
				if (bodyPart.getFileName() != null) {
					fileName = MimeUtility.decodeText(bodyPart.getFileName());
				}
				POP3Logger.info("  attr file name ***" + fileName + "***");
				String ftpDirName = getAttachmtDirName(mail);

				if (content_id != null
						&& disposition.equalsIgnoreCase(BodyPart.INLINE)) {
					ftpDirName = getInlineImageDirName(mail);

					if (mail.getContent() != null) {
						mail.setContent(mail.getContent().replace(
								content_id,
								"/koyoo-email-provider/download/image/"
										+ EncodingExchange
												.stringToHex(ftpDirName
														+ fileName)));
					}

				} else {
					ftpDirName = getAttachmtDirName(mail);
				}

				InputStream is = bodyPart.getInputStream();
				if (null == is) {
					POP3Logger.error("Attachment is null.");
					continue;
				}
				mail.setAttachmtDir(ftpDirName);
				POP3Logger.info(String.format("ftpDirName: %s, fileName: %s",
						ftpDirName, fileName));

				if (!uploadAttachmt(is, ftpDirName, fileName)) {
					POP3Logger
							.error(String
									.format("File %s transfer failed with FTP server directory path %s, sent by: %s, subject: %s.",
											fileName, ftpDirName,
											mail.getSender(), mail.getSubject()));
				}

			} else if (bodyPart.isMimeType(TEXT_HTML_CONTENT)) {
				mail.setContent((String) bodyPart.getContent());
			} else if (bodyPart.isMimeType(TEXT_PLAIN_CONTENT)) {
				// TO DO

			} else if (bodyPart.isMimeType(MULTIPART)) {
				parseMultipartByMimeType((Multipart) bodyPart.getContent(),
						mail);
			}
		}

	}

	public static boolean uploadAttachmt(InputStream is, String dirName,
			String fileName) {
		FTPClient ftp = null;
		try {
			ftp = FTPConnectionFactory.getDefaultFTPConnection();
			boolean success = FTPEngine.uploadFile(ftp, dirName, fileName, is);
			ftp.logout();
			return success;
		} catch (Exception e) {
			POP3Logger.warn("Ftp exception: " + e.toString());
			return false;
		} finally {
			if (null != ftp) {
				try {
					ftp.disconnect();
					POP3Logger.info("Ftp disconnected.");
				} catch (IOException e) {
					POP3Logger.warn(e.toString());
					e.printStackTrace();
				}
			}
			ftp = null;
		}

	}

	public static String getAttachmtDirName(Mail mail) {
		Date date = mail.getSentDate();
		String uid = mail.getUid();
		if (uid.indexOf(":") != -1)
			uid = uid.substring(0, uid.indexOf(":"));
		String attachmtDirName = ATTR_TOP_DIR + "/" + mail.getReceiver() + "/"
				+ TimeHelper.getYearBaseDate(date) + "/"
				+ TimeHelper.getMonthBaseDate(date) + "/"
				+ TimeHelper.getDayBaseDate(date) + "/" + uid + "/";
		return attachmtDirName;
	}

	public static String getInlineImageDirName(Mail mail) {
		Date date = mail.getSentDate();
		String uid = mail.getUid();
		if (uid.indexOf(":") != -1)
			uid = uid.substring(0, uid.indexOf(":"));
		String imageDirName = ATTR_TOP_DIR + "/" + mail.getReceiver() + "/"
				+ TimeHelper.getYearBaseDate(date) + "/"
				+ TimeHelper.getMonthBaseDate(date) + "/"
				+ TimeHelper.getDayBaseDate(date) + "/" + uid
				+ "/Inline_images/";
		return imageDirName;
	}

	/**
	 * 获取邮件内的图片等ID
	 * 
	 * @param p
	 * @return
	 * @throws MessagingException
	 */
	public static String getContentid(Part p) throws MessagingException {
		String cidraw = null, cid = null;

		String[] headers = p.getHeader("Content-id");
		if (headers != null && headers.length > 0) {
			cidraw = headers[0];
		} else {
			return null;
		}
		if (cidraw.startsWith("<") && cidraw.endsWith(">")) {
			cid = "cid:" + cidraw.substring(1, cidraw.length() - 1);
		} else {
			cid = "cid:" + cidraw;
		}
		return cid;

	}

	/**
	 * 获取POP3协议的邮件仓库
	 * 
	 * @param a
	 *            邮箱账户对象
	 * @return POP3 mail store
	 * @throws MessagingException
	 */
	public static Store POP3connection(Account a) throws MessagingException {

		Session session = Session.getInstance(PropertiesFactory.POP3Factory(a));
		Store store = session.getStore(POP3);
		store.connect(a.getAccount(), a.getPassword());
		POP3Logger.info("Build POP3 connection, Acccount = " + a.getAccount()
				+ " Password = " + a.getPassword());
		return store;
	}

	public static POP3Folder getPOP3Inbox(Store s) throws MessagingException {
		POP3Folder inbox = (POP3Folder) s.getFolder(INBOX);
		inbox.open(POP3Folder.READ_WRITE);
		return inbox;
	}

	/**
	 * 断开POP3连接
	 * 
	 * @param s
	 *            POP3 mail store
	 * @param inbox
	 *            POP3Folder
	 * @throws MessagingException
	 */
	public static void closePOP3connection(Store s, POP3Folder inbox)
			throws MessagingException {
		if (inbox != null && inbox.isOpen()) {
			POP3Logger.info("Close inbox.");
			inbox.close(true);
		}
		if (s != null && s.isConnected()) {
			POP3Logger.info("Close store.");
			s.close();
		}
	}

	/**
	 * 断开POP3连接
	 * 
	 * @param s
	 *            POP3 mail store
	 * @param inbox
	 *            java.mail.Folder
	 * @throws MessagingException
	 */
	public static void closePOP3connection(Store s, Folder inbox) {
		try {
			if (inbox != null && inbox.isOpen()) {
				POP3Logger.info("Close inbox.");
				inbox.close(true);
			}
			if (s != null && s.isConnected()) {
				POP3Logger.info("Close store.");
				s.close();
			}
		} catch (MessagingException e) {
			s = null;
			inbox = null;
			POP3Logger.error(e.toString());
			e.printStackTrace();
		}
	}

	/**
	 * 根据UID获取邮件
	 * 
	 * @param account
	 *            Email账户
	 * @param uid
	 *            UID
	 * @return 在Inbox中与给定UID对应的邮件
	 * @throws MessagingException
	 */
	public static Message getMessageByUID(Account account, String uid)
			throws MessagingException {
		Store store = null;
		POP3Folder inbox = null;

		try {
			store = POP3connection(account);
			inbox = getPOP3Inbox(store);
		} catch (MessagingException e) {
			closePOP3connection(store, inbox);
			POP3Logger.info("Build POP3 connection failed, caused by: "
					+ e.toString());
			return null;
		}

		POP3Logger.info("POP3 connected for account: " + account.getAccount());

		Message[] messages = inbox.getMessages();
		for (Message msg : messages) {
			if (inbox.getUID(msg).equals(uid)) {
				return msg;
			}
		}
		closePOP3connection(store, inbox);
		return null;
	}

	/**
	 * 返回收件箱中邮件总数，并关闭回话
	 * 
	 * @param account
	 *            邮件账户
	 * @return 收件箱中邮件数量
	 * @throws MessagingException
	 */
	public static int getInboxCount(Account account) throws MessagingException {
		Store s = POP3connection(account);
		POP3Folder inbox = getPOP3Inbox(s);
		int count = inbox.getMessageCount();
		closePOP3connection(s, inbox);
		return count;
	}

}

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<script>
	dojoConfig = {
		parseOnLoad : true
	}
</script>
<link rel="stylesheet"
	href="<c:url value='/resources/js/libs/dojo/1.9.1/dijit/themes/claro/claro.css'/>" />
<script
	src="<c:url value='/resources/js/libs/dojo/1.9.1/dojo/dojo.js'/>"></script>
<script>
	dojo.require("dijit/form/ValidationTextBox");
	dojo.require("dojo.on");
	dojo.require("dojo.io-query");

	function saveSetting() {
		var save = dojo.byId("saveSetting");
		dojo.on(save, "click", function() {

			var host = dojo.byId("host").value;
			var port = dojo.byId("port").value;
			var username = dojo.byId("username").value;
			var password = dojo.byId("password").value;
			console.log("host: " + host + " username: " + username
					+ " password: " + password);
			var begin = dojo.byId("begin").value;
			var end = dojo.byId("end").value;
			var interval = dojo.byId("interval").value;
			console.log(begin + " " + end);
			if (parseInt(begin) >= parseInt(end)) {
				console.log("Error.");
				alert("请正确填写开始时间和结束时间。");
				return false;
			}

			var queryStr;
			require([ "dojo/io-query" ], function(ioQuery) {
				var query = {
					ftp : dojo.toJson({
						"host" : host,
						"port" : port,
						"username" : username,
						"password" : password
					}),
					runner : dojo.toJson({
						"begin" : begin,
						"end" : end,
						"interval" : interval
					})
				};
				// Assemble the new uri with its query string attached.
				queryStr = ioQuery.objectToQuery(query);

			});
			console.log("queryStr: " + queryStr);
			var xhrArgs = {
				url : "/koyoo-email-provider/setting/save",
				postData : queryStr,
				handleAs : "text",
				load : function(data) {
					console.log("res:" + data);
				},
				error : function(eror) {
					console.log("error:" + error);
				}
			}
			var deferred = dojo.xhrPost(xhrArgs);
		});
	}

	dojo.ready(function() {
		saveSetting();
		var runButton = dojo.byId("run");
		dojo.on(runButton, "click", function() {
			require([ "dojo/request" ], function(request) {
				request("/koyoo-email-provider/setting/run").then(
						function(reply) {
							console.log("Running reply: " + reply);
						});
			});
		});

		var stopButton = dojo.byId("stop");
		dojo.on(stopButton, "click", function() {
			require([ "dojo/request" ], function(request) {
				request("/koyoo-email-provider/setting/stop").then(
						function(reply) {
							console.log("Stop reply: " + reply);
						});
			});
		});

		var pauseButton = dojo.byId("pause");
		dojo.on(pauseButton, "click", function() {
			require([ "dojo/request" ], function(request) {
				request("/koyoo-email-provider/setting/pause").then(
						function(reply) {
							console.log("Pause reply: " + reply);
						});
			});
		});

		var resumeButton = dojo.byId("resume");
		dojo.on(resumeButton, "click", function() {
			require([ "dojo/request" ], function(request) {
				request("/koyoo-email-provider/setting/resume").then(
						function(reply) {
							console.log("Resume reply: " + reply);
						});
			});
		});
	});
</script>
</head>
<body class="claro">

	<script type="dojo/on" data-dojo-event="reset">
        return confirm('Press OK to reset widget values');
    </script>

	<script type="dojo/on" data-dojo-event="submit">
        if(this.validate()){
            return confirm('Form is valid, press OK to submit');
        }else{
            alert('Form contains invalid data.  Please correct first');
            return false;
        }
        return true;
    </script>
	<h4>FTP配置</h4>
	<table style="border: 1px solid #9f9f9f;">
		<tr>
			<td><label>主机地址:</label></td>
			<td><input type="text" id="host" name="host" required="true"
				value="${host}" data-dojo-type="dijit/form/ValidationTextBox" /></td>
			<td><label>端口号:</label></td>
			<td><input type="text" id="port" name="port" required="true"
				value="${port}" data-dojo-type="dijit/form/ValidationTextBox" /></td>
		</tr>
		<tr>
			<td><label>用户名:</label></td>
			<td><input type="text" id="username" name="username"
				required="true" data-dojo-type="dijit/form/ValidationTextBox"
				value="${username}" /></td>
		</tr>
		<tr>
			<td><label>密码:</label></td>
			<td><input type="text" id="password" name="password"
				value="${password}" required="true"
				data-dojo-type="dijit/form/ValidationTextBox" /></td>
		</tr>
	</table>

	<h4>POP3协议邮件抓取配置</h4>
	<h5 style="color: red">
		设置开始时间和结束时间来定义每天抓取邮件的时间段,请填写星号(*)或者0到23之间的数字。<br />例如设置开始时间为8，结束时间为22，系统将在每天早上八点到晚上十点之间自动抓取邮件。<br />都设置为*时，系统将全天候不间断抓取邮件。
	</h5>
	<table style="border: 1px solid #9f9f9f;">
		<tr>
			<td><label>收取邮箱时间间隔:</label></td>
			<td><input type="text" id="interval" name="interval"
				value="${interval}" style="width:20px" required="true"
				data-dojo-type="dijit/form/ValidationTextBox" />分钟</td>
		</tr>
		<tr>
			<td><label>开始时间:</label></td>
			<td><input type="text" id="begin" name="begin" value="${begin}"
				style="width:20px" required="true"
				data-dojo-type="dijit/form/ValidationTextBox" />时</td>
		</tr>
		<tr>
			<td><label>结束时间:</label></td>
			<td><input type="text" id="end" name="end" value="${end}"
				style="width:20px" required="true"
				data-dojo-type="dijit/form/ValidationTextBox" />时</td>
		</tr>

	</table>

	<button id="saveSetting" name="saveSetting">保存</button>
	<button id="run" name="run">运行</button>
	<button id="stop" name="stop">停止</button>
	<button id="pause" name="pause">暂停</button>
	<button id="resume" name="resume">恢复</button>
</body>
</html>
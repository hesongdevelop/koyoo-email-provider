<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/mailViewer.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/buttonStyle.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/js/libs/dojo/1.9.1/dojox/layout/resources/ScrollPane.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/js/libs/dojo/1.9.1/dijit/themes/claro/claro.css'/>" />

<script>
	dojoConfig = {
		parseOnLoad : true
	}
</script>
<script
	src="<c:url value='/resources/js/libs/dojo/1.9.1/dojo/dojo.js'/>">
	
</script>

<script>
	dojo.require("dijit.Editor");
	dojo.require("dijit._editor.plugins.TextColor");
	dojo.require("dijit._editor.plugins.LinkDialog");
	dojo.require("dojo.on");
	dojo.require("dojo.io-query");
	dojo.require("dojox.widget.Standby");

	function sendMail() {

		var button = dojo.byId("showReplyButton");
		var sendingStandby = new dojox.widget.Standby({
			target : "replyBox",
			image : "../../../resources/images/sending.gif"
		});

		var sendingOk = new dojox.widget.Standby({
			target : "replyBox",
			image : "../../../resources/images/send-ok.png"
		});

		var sendingERR = new dojox.widget.Standby({
			target : "replyBox",
			image : "../../../resources/images/send-err.png"
		});

		document.body.appendChild(sendingStandby.domNode);
		document.body.appendChild(sendingOk.domNode);
		document.body.appendChild(sendingERR.domNode);
		sendingStandby.startup();
		sendingOk.startup();
		sendingERR.startup();
		dojo.on(button, "click", function(event) {

			sendingStandby.show();
			var reply_content = dojo.byId("reply-content");
			var content = dijit.byId('replyBox').get('value');
			reply_content.value = content;
			dojo.byId("myForm").submit();

		});
	}

	dojo.ready(function() {

		require([ "dojox/layout/ScrollPane" ], function(ContentPane) {
			new ContentPane({
				orientation : "vertical",
				style : "height:100%; overflow:hidden;"
			}).placeAt("mailContent");
		});

		var mailEditor = new dijit.Editor(
				{
					height : "200px",
					extraPlugins : [ '|', 'foreColor', 'hiliteColor', '|',
							'createLink' ],
					onChange : function() {
						console.log('Editor box: '
								+ dijit.byId('replyBox').get('value'));
					}
				}, "replyBox");
		dojo.style(dojo.byId('loadingOverlay'), "display", "none");
		mailEditor.startup();
		dojo.style(dojo.byId('showReplyButton'), "display", "block");
		sendMail();
	})
</script>
</head>
<body>
	<div id="mailContainer">
		<div id="loadingOverlay" class="loadingOverlay pageOverlay">
			<div class="loadingMessage">加载中...</div>
		</div>
		<div id="mailViewer">

			<p>
				<span class="bold">发信人：</span><span id="senderAdrBox">${mail.sender}</span>
			</p>
			<p>
				<span class="bold">发送时间：</span><span id="sentDateBox">${fn:substring(mail.sentDate,0,19) }</span>
				<input type="hidden" value="${mail.id }" id="pre-mid" />
			</p>
			<p>
				<span class="bold">收件人：</span><span id="receiverBox">${mail.receiver}</span>
				<span id="uidBox" style="display: none">${mail.uid}</span>
			</p>
			<p>
				<span class="bold">主题：</span><span id="subjectBox">${mail.subject}</span>
			</p>


			<c:if test="${not empty attchmtFiles}">
				<div id="attchmtFilesBox">
					<ul style="list-style-type: none; padding: 1px; margin: 0;">
						<li style="display: inline"><span
							style="color: #8c8c8c; font-size: 12px; font-weight: bold;">附件<img
								style="height: 20px; width: 20px; position: relative; top: 5px"
								src="<c:url value='/resources/images/attachment.png'/>">
						</span></li>
						<c:forEach var="attchmtFile" items="${attchmtFiles}">
							<li id="attachmtList"
								style="display: inline; margin-right: 5px; margin-bottom: 5px">
								<a class="attchmt_btn"
								style="display: inline-block; margin-bottom: 5px;"
								title="Size: ${attchmtFile.size}"
								href="<%=basePath %>download/attachment/${attchmtFile.dirPath}">${attchmtFile.fileName}
							</a>
							</li>
						</c:forEach>
					</ul>
				</div>
			</c:if>
			<hr noshade size=1 style="color: red;" />
			<div id="mailContent">${mail.content}</div>

			<form method="post" id="myForm" enctype="multipart/form-data"
				action="<%=basePath%>newMail/reply">
				<input type="hidden" name="subject" value="${mail.subject }">
				<input type="hidden" name="receiver" value="${mail.receiver }">
				<input type="hidden" name="sender" value="${mail.sender }">
				<input type="hidden" name="orgin_id" value="${mail.id  }"> <input
					type="hidden" name="orgin_send_date" value="${mail.sentDate }">
				<input type="hidden" name="orgin_sender" value=${mail.receiver }>

				<textarea rows="100" cols="100" style="display: none"
					id="reply-content" name="content"></textarea>
				<textarea rows="100" cols="100" style="display: none"
					name="orgin_content">${mail.content }</textarea>

				<div class="claro" style="margin-top: 10px">
					<div id="replyBox"></div>
					<h5>
						选择附件（可选）： <input name="uploadedfile" multiple="true" type="file"
							data-dojo-type="dojox.form.Uploader" id="uploader" value="请选择附件" />
					</h5>
					<div id="files" data-dojo-type="dojox.form.uploader.FileList"
						data-dojo-props='uploaderId:"uploader"'></div>
					<div style="height: 40px">
						<a
							style="margin-top: 10px; margin-right: 2px; float: right; display: none"
							href="#" id="showReplyButton" class="replyButton">回复</a>
					</div>
				</div>
			</form>

		</div>
	</div>
</body>
</html>
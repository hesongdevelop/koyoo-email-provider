<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/mailViewer.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/buttonStyle.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/js/libs/dojo/1.9.1/dojox/layout/resources/ScrollPane.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/js/libs/dojo/1.9.1/dijit/themes/claro/claro.css'/>" />

<script
	src="<c:url value='/resources/js/libs/dojo/1.9.1/dojo/dojo.js'/>">
	<script>
	dojoConfig = {
		parseOnLoad : true
	}
</script>

<script>
	dojo.require("dijit.Editor");
	dojo.require("dijit._editor.plugins.TextColor");
	dojo.require("dijit._editor.plugins.LinkDialog");
	dojo.require("dojo.on");
	dojo.require("dojo.cookie");
	dojo.require("dojox.widget.Standby");

	var emailAdrsCheck = false;

	function checkEmailAdrs() {
		var emailInput = dojo.byId("receiver");
		dojo.on(emailInput, "change", function(event) {
			var emailAdrs = emailInput.value;
			console.log("email changed: " + emailAdrs);
			require([ "dojox/validate/web" ], function(validate) {
				var isEmailAddress = validate.isEmailAddress(emailAdrs);
				if (!isEmailAddress) {
					emailAdrsCheck = false;
					dojo.style(emailInput, "border", "1px solid red");
				} else {
					emailAdrsCheck = true;
					dojo.style(emailInput, "border", "");
				}
			});
		});
	}

	function sendMail() {
		var button = dojo.byId("sendMailButton");
		var sendingStandby = new dojox.widget.Standby({
			target : "replyBox",
			image : "../../resources/images/sending.gif"
		});

		var sendingOk = new dojox.widget.Standby({
			target : "replyBox",
			image : "../../resources/images/send-ok.png"
		});

		var sendingERR = new dojox.widget.Standby({
			target : "replyBox",
			image : "../../resources/images/send-err.png"
		});
		document.body.appendChild(sendingStandby.domNode);
		document.body.appendChild(sendingOk.domNode);
		document.body.appendChild(sendingERR.domNode);
		sendingStandby.startup();
		sendingOk.startup();
		sendingERR.startup();

		dojo.on(button, "click", function(event) {

			var email_content = dojo.byId("email-content");
			var content = dijit.byId('replyBox').get('value');
			email_content.value = content;

			if (subject == "" || content == "" || receiver == "") {
				alert("请按要求填写");
			} else {
				sendingStandby.show();
				dojo.byId("myForm").submit();
			}

		});
	}

	dojo.ready(function() {
		sendMail();
		checkEmailAdrs();
		require([ "dojox/layout/ScrollPane" ], function(ContentPane) {
			new ContentPane({
				orientation : "vertical",
				style : "height:100%; overflow:hidden;"
			}).placeAt("mailContent");
		});

		var mailEditor = new dijit.Editor(
				{
					height : "400px",
					extraPlugins : [ '|', 'foreColor', 'hiliteColor', '|',
							'createLink' ]
				}, "replyBox");
		mailEditor.startup();
		dojo.style(dojo.byId('showReplyButton'), "display", "block");
	});

</script>
</head>
<body>

	<div id="mailContainer">
		<form method="post" id="myForm" enctype="multipart/form-data" action="<%=basePath%>newMail/send">
			<div id="mailViewer">
				<input type="hidden" value="${sender_email }" id="sender-mail"
					name="sender">
				<textarea rows="100" cols="100" style="display: none"
					id="email-content" name="content"></textarea>
				<h5 id="subjectBox">
					主&nbsp;&nbsp;&nbsp;题：<input type="text" id="subject"
						value="${subject }" name="subject"
						style="display: inline-block;width: 600px;height: 26px;border:none;border-bottom:2px solid #000" />
				</h5>

				<h5 style="width: 100%">
					收件人：<span id="senderAdrBox" style="color: #47f54d"> <input
						id="receiver" type="text" name="receivers" value="${address }"
						style="display: inline-block;width: 600px;height: 26px;border:none;border-bottom:2px solid #000" />
					</span>
				</h5>


				<div id="mailContent"></div>
				<div class="claro">
					<div id="replyBox"></div>
					<h5>
						选择附件（可选）： <input name="uploadedfile" multiple="true" type="file"
							data-dojo-type="dojox.form.Uploader" id="uploader" value="请选择附件" />
					</h5>
					<div id="files" data-dojo-type="dojox.form.uploader.FileList"
						data-dojo-props='uploaderId:"uploader"'></div>
					<div style="height: 40px">
						<a style="margin-top: 10px; margin-right: 2px; float: right;"
							href="#" id="sendMailButton" class="replyButton">发送</a>
					</div>
				</div>

			</div>
		</form>
	</div>
</body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet"
	href="<c:url value='/resources/css/mailViewer.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/buttonStyle.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/js/libs/dojo/1.9.1/dojox/layout/resources/ScrollPane.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/js/libs/dojo/1.9.1/dijit/themes/claro/claro.css'/>" />
</head>

<body>
	<div id="mailContainer">
		<div id="mailViewer">

			<p>
				<span class="bold">发件人：</span>${mail.sender}</p>
			<p>
				<span class="bold">发送时间：</span>${fn:substring(mail.sentDate,0,19)}</p>
			<p>
				<span class="bold">收件人：</span>${mail.receiver}</p>
			<p>
				<span class="bold">主题：</span>${mail.subject}</p>
			<c:if test="${not empty attchmtFiles}">
				<div id="attchmtFilesBox">
					<ul style="list-style-type: none; padding: 1px; margin: 0;">
						<li style="display: inline"><span
							style="color: #8c8c8c; font-size: 12px; font-weight: bold;">附件<img
								style="height: 20px; width: 20px; position: relative; top: 5px"
								src="<c:url value='/resources/images/attachment.png'/>">
						</span></li>
						<c:forEach var="attchmtFile" items="${attchmtFiles}">
							<li id="attachmtList"
								style="display: inline; margin-right: 5px; margin-bottom: 5px">
								<a class="attchmt_btn"
								style="display: inline-block; margin-bottom: 5px;"
								title="Size: ${attchmtFile.size}"
								href="<%=basePath %>download/attachment/${attchmtFile.dirPath}">${attchmtFile.fileName}
							</a>
							</li>
						</c:forEach>
					</ul>
				</div>
			</c:if>
			<hr noshade size=1 style="color: red;" />
			<div id="mailContent">${mail.content}</div>

			<div id="reply">
				<c:if test="${empty reply }">
					<h3 class="title">尚未回复</h3>
				</c:if>
				<c:if test="${!empty reply }">
					<div style="font-size: 18px;font-weight: bold;">&rarr;回&nbsp;&nbsp;&nbsp;信：</div>
					<p style="color: #009AFF">
						<span style="font-weight: bold;color:#000">回件人：</span>${reply.sender}</p>
					<p style="color: #009AFF">
						<span style="font-weight: bold;color:#000"">回复时间：</span>${fn:substring(reply.sentDate,0,19)}</p>
					<c:if test="${not empty r_attchmtFiles}">
						<div id="attchmtFilesBox">
							<ul style="list-style-type: none; padding: 1px; margin: 0;">
								<li style="display: inline"><span
									style="color: #8c8c8c; font-size: 12px; font-weight: bold;">附件<img
										style="height: 20px; width: 20px; position: relative; top: 5px"
										src="<c:url value='/resources/images/attachment.png'/>">
								</span></li>
								<c:forEach var="attchmtFile" items="${r_attchmtFiles}">
									<li id="attachmtList"
										style="display: inline; margin-right: 5px; margin-bottom: 5px">
										<a class="attchmt_btn"
										style="display: inline-block; margin-bottom: 5px;"
										title="Size: ${attchmtFile.size}"
										href="<%=basePath %>download/attachment/${attchmtFile.dirPath}">${attchmtFile.fileName}
									</a>
									</li>
								</c:forEach>
							</ul>
						</div>
					</c:if>
					<hr />
					<div class="content">${reply.content}</div>
				</c:if>
			</div>
		</div>
	</div>

</body>
</html>

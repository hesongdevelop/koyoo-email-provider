<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>邮件已发送成功</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<style type="text/css">
div {
	text-align: center;
}

.main {
	width: 700px;
	height: 500px;
}

.tip {
	width: 100%;
	color: red;
	margin: 100px 0 50px 0;
	font-size: 30px;
	font-weight: bold;
}
.reason{
	text-align: left;
}
</style>
</head>

<body>
	<div class="main">
		<div class="tip">邮件发送失败</div>
		<div>
			<img alt="发送成功" src="<%=basePath%>resources/images/send-err.png">
		</div>
		<div class="reason">
			请检查：
			<ol>
				<li>当前计算机网络是否通畅</li>
				<li>您所填写的收件人或者内容是否有误</li>
				<li>您发送的附件是否过大(最大20M)</li>
			</ol>
		</div>
	</div>
</body>
</html>

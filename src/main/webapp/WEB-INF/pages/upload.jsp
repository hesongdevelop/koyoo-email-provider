<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet"
	href="<c:url value='/resources/js/libs/dojo/1.9.1/dijit/themes/dijit.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/js/libs/dojo/1.9.1/dijit/themes/claro/Common.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/js/libs/dojo/1.9.1/dijit/themes/claro/form/Common.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/js/libs/dojo/1.9.1/dijit/themes/claro/form/claro.css'/>" />

<script src="<c:url value='/resources/js/libs/dojo/1.9.1/dojo/dojo.js'/>"></script>

<script>
    dojo.require("dojox.form.Uploader");
    dojo.require("dojox.form.uploader.FileList");
    dojo.require("dijit.form.Button");
    dojo.require("dojo.request.iframe");
    dojo.ready(function() {
    	require(["dojo/request/iframe"],function(iframe){
        iframe("upload/attachmt",{
            form: "myForm",
            handleAs: "text"
        }).then(function(data){
            console.log("data: "+data);
        });
    	});
    });
</script>
</head>
<body>
	<form method="post" id="myForm" enctype="multipart/form-data">
		<input name="uploadedfile" multiple="true" type="file"
			data-dojo-type="dojox.form.Uploader" id="uploader" /> <input
			type="submit" data-dojo-type="dijit.form.Button" />
		<div id="files" data-dojo-type="dojox.form.uploader.FileList"
			data-dojo-props='uploaderId:"uploader"'></div>
	</form>
</body>
</html>
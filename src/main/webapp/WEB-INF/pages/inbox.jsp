<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <link rel="stylesheet" href="<c:url value='/resources/js/libs/dojo/1.9.1/dijit/themes/claro/claro.css'/>" />
    <link rel="stylesheet" href="<c:url value='/resources/js/libs/dojo/1.9.1/dojo/resources/dojo.css'/>" />
    <link rel="stylesheet" href="<c:url value='/resources/js/libs/dojo/1.9.1/dijit/themes/claro/claro.css'/>" />
    <link rel="stylesheet" href="<c:url value='/resources/js/libs/dojo/1.9.1/dojox/grid/resources/Grid.css'/>" />
    <link rel="stylesheet" href="<c:url value='/resources/js/libs/dojo/1.9.1/dojox/grid/resources/claroGrid.css'/>" />
    <style type="text/css">
@import "<c:url value='/resources/js/libs/dojo/1.9.1/dojo/resources/dojo.css'/>";
@import "<c:url value='/resources/js/libs/dojo/1.9.1/dijit/themes/claro/claro.css'/>";
@import "<c:url value='/resources/js/libs/dojo/1.9.1/dojox/grid/enhanced/resources/claro/EnhancedGrid.css'/>";
@import "<c:url value='/resources/js/libs/dojo/1.9.1/dojox/grid/enhanced/resources/EnhancedGrid_rtl.css'/>";

/*Grid need a explicit width/height by default*/
#grid {
    width: 60em;
    height: 30em;
}
    </style>
    <script>dojoConfig = {parseOnLoad: true}</script>

<script src="<c:url value='/resources/js/libs/dojo/1.9.1/dojo/dojo.js'/>">
</script>
<script>
typeof(dojo) === "undefined" && document.write(unescape('%3Cscript src="../js/libs/dojo/1.9.1/dojo/dojo.js"%3E%3C/script%3E'))
</script>    

    <script>
      
    dojo.require("dojox.grid.EnhancedGrid");
    dojo.require("dojo.store.JsonRest");
    dojo.require("dojo.data.ObjectStore");
    dojo.require("dojo.date.locale");
    dojo.require("dojox.grid.enhanced.plugins.Pagination");
    dojo.require("dojox.grid.enhanced.plugins.NestedSorting");
    dojo.require("dojo.store.Memory");
    dojo.require("dojo.store.Cache");
   
    formatDate = function(inDatum){
        return dojo.date.locale.format(new Date(inDatum), this.constraint);
    }

    dojo.ready(function(){

        var jsonStore = new dojo.store.JsonRest({
            target: "inbox/resources",
        });
        
        var store = dojo.data.ObjectStore({objectStore: jsonStore});
        /*set up layout*/
        var layout = [[
          {'name': 'ID', 'field': 'id', 'width': '40px', 'hidden':'true'},
          {'name': 'Sender', 'field': 'senderName', 'width': '230px'},
          {'name': 'Subject', 'field': 'subject', 'width': '230px'},
          {'name': 'Sent Date', 'field': 'sentDate', 'width': '230px',
              formatter: formatDate, 
              constraint: {datePattern: "yyyy/MM/dd", timePattern: "HH:mm:ss"}}
        ]];

        /*create a new grid:*/
        var grid = new dojox.grid.EnhancedGrid({
            id: 'grid',
            store: store,
            structure: layout,
            rowSelector: '20px',
            plugins: {
            	pagination: {
                pageSizes: ["25", "50", "100", "All"],
                description: true,
                sizeSwitch: true,
                pageStepper: true,
                gotoButton: true,
                        /*page step to be displayed*/
                maxPageStep: 4,
                        /*position of the pagination bar*/
                position: "bottom"
            },
            nestedSorting: true,
          }
        },
          document.createElement('div'));

        /*append the new grid to the div*/
        dojo.byId("gridDiv").appendChild(grid.domNode);

        /*Call startup() to render the grid*/
        grid.startup();
    });
    </script>
</head>
<body class="claro">
    <div id="gridDiv"></div>
    
</body>
</html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>邮件已发送成功</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<style type="text/css">
div {
	text-align: center;
}

.main {
	width: 700px;
	height: 500px;
}

.tip {
	width: 100%;
	color: #000;
	margin: 100px 0 50px 0;
	font-size: 30px;
	font-weight: bold;
}
</style>
</head>

<body>
	<div class="main">
		<div class="tip">您的邮件已经发送成功</div>
		<div>
			<img alt="发送成功" src="<%=basePath%>resources/images/send-ok.png">
		</div>
	</div>
</body>
</html>
